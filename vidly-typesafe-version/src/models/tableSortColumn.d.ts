interface TableSortColumn {
  path: string;
  order: "asc" | "desc";
}
