/**
 * Use this to override some properties from an interface.
 * @param T - The interface to change
 * @param O - The keys to override and their new type
 * @example
 * interface IOriginal {
 *  name: string;
 *  address: string;
 * }
 * interface IModified extends Override<IOriginal, { name: number }> {}
 *
 * // Creates the following
 * const fields: IModified = {
 *  name: 123, // must now be a number
 *  address: 'must be a string'
 * }
 */
type Override<T, O extends { [P in keyof Partial<T>]: unknown }> = Omit<
  T,
  keyof O
> &
  O;
