import React from "react";
import { Link } from "react-router-dom";
import { ListGroup, ListGroupItem } from "../components/common/listGroup";
import { Search } from "../components/common/search";
import { MoviesTable } from "../components/table/moviesTable";
import { genres, getGenres } from "../services/fakeGenreService";
import * as MovieService from '../services/fakeMovieService';

export type Genre = MovieService.MovieEntity['genre']
export interface MoviesPageState {
    movies: Array<MovieService.MovieEntity>;
    genres: Array<Genre>;
    selectedGenre: Genre | null;
    searchQuery: string;
}

export class MoviesPage extends React.Component<{}, MoviesPageState> {
    state: MoviesPageState = { movies: [], genres: [], selectedGenre: null, searchQuery: '' }
    movieService = MovieService;

    handleDelete(movie: MovieService.MovieEntity) {
        this.setState({
            movies: this.state.movies.filter(m => m._id !== movie._id)
        })
    }

    handleLike(movie: MovieService.MovieEntity) {
        const movies = [...this.state.movies];
        const theMovie = movies.find(m => m._id === movie._id);
        if (!theMovie) return;

        theMovie.liked = !movie.liked;
        this.setState({
            movies
        })
    }

    handleSearch(query: string) {
        this.setState({
            searchQuery: query,

        })
    }

    handleItemChange(item: ListGroupItem | null) {
        this.setState({
            selectedGenre: genres.find(g => g._id === item?.value) || null,
            searchQuery: '' // reset search query after changing genre
        })
    }

    getMovies() {
        let moviesToReturn = this.state.movies;

        if (this.state.selectedGenre) {
            const selectedGenreId = this.state.selectedGenre._id;
            moviesToReturn = moviesToReturn.filter(m => m.genre._id === selectedGenreId);
        }
        if (this.state.searchQuery) {
            moviesToReturn = moviesToReturn.filter(m => m.title.toLowerCase().includes(this.state.searchQuery));
        }

        return moviesToReturn;
    }

    componentDidMount() {
        const movies = this.movieService.getMovies();
        this.setState({
            movies,
            genres: getGenres()
        })
    }

    render() {
        const genreListItems = this.state.genres.map(genre => ({ value: genre._id, text: genre.name }))
        const { selectedGenre } = this.state;
        const selectedGenreItem: ListGroupItem | null = !selectedGenre ? null : { value: selectedGenre?._id, text: selectedGenre?.name };

        return (
            <React.Fragment>
                <Link to="/movies/new" className="btn btn-primary">Add new movie</Link>
                <Search onChange={this.handleSearch.bind(this)} value={this.state.searchQuery} />
                <ListGroup
                    items={genreListItems}
                    nullableLabel="Display all genres"
                    onItemChange={this.handleItemChange.bind(this)}
                    selectedItem={selectedGenreItem} />
                <MoviesTable
                    movies={this.getMovies()}
                    onDelete={this.handleDelete.bind(this)}
                    onLike={this.handleLike.bind(this)} />
            </React.Fragment>
        );
    }

}