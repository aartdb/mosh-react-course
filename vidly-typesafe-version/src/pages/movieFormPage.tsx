import React from "react";
import { RouteComponentProps } from 'react-router-dom'
import queryString from "query-string";
import Joi from "joi";
import { Input } from "../components/common/input";
import { FormService } from "../services/formService";
import { addMovie, getMovie, MovieEntity, saveMovie } from "../services/fakeMovieService";
import { ListGroup } from "../components/common/listGroup";
import { getGenres } from "../services/fakeGenreService";

// We want to access the route params from the url, so we need to import the RouteComponentProps interface.
interface MovieFormPageProps extends RouteComponentProps {

}

interface MovieFormPageState {
    data: Omit<MovieEntity, '_id'>;
    validationErrors: Partial<Record<keyof MovieEntity, string>>;
}

export class MovieFromPage extends React.Component<MovieFormPageProps, MovieFormPageState> {
    queryParams = queryString.parse(this.props.location?.search || '');

    state: MovieFormPageState = {
        data: {
            title: '',
            genre: { _id: '', name: '' },
            numberInStock: 0,
            dailyRentalRate: 0
        }, validationErrors: {}
    };

    schema = Joi.object<MovieEntity>({
        // Don't validate
        _id: Joi.allow(),
        genre: Joi.allow(),

        title: Joi.string().required().label('Title'),
        numberInStock: Joi.number().min(0).required().label('Number in Stock'),
        dailyRentalRate: Joi.number().min(0).required().label('Daily Rental Rate')
    });
    movieToEdit?: MovieEntity;
    formService = new FormService(this.schema);
    genres = getGenres();

    componentDidMount() {
        const movieId = (this.props.match.params as any).id;
        if (movieId === 'new') {
            // stop this method from running
            this.setState({ data: { ...this.state.data, genre: this.genres[0] } });
            return;
        }

        if (movieId) {
            const movie = getMovie(movieId);
            if (!movie) return this.props.history.replace("/not-found");

            this.movieToEdit = movie;
            if (this.movieToEdit) {
                this.setState({ data: this.movieToEdit });
            }
        }
    }

    handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        const errors = this.formService.validateForm(this.state.data);
        this.setState({ validationErrors: errors || {} });
        console.log('submitting form with errors', errors);
        if (errors) return;
        console.log('submitting form');

        this.handleSave();
    }

    handleChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
        const input = e.target;
        const name = input.name as keyof MovieEntity;
        const value = input.value;

        this.setState({ ...this.state, data: { ...this.state.data, [name]: value } });

        const errors = { ...this.state.validationErrors };
        const errorMessage = this.formService.validateField(name, value);
        errors[name] = errorMessage ?? undefined;

        this.setState({ validationErrors: errors });
        return true;
    }

    renderInput(name: keyof Omit<MovieEntity, 'genre' | '_id'>, label: string, type = 'text') {
        return (
            <Input
                label={label}
                name={name}
                type={type}
                value={this.state.data[name] as string}
                onChange={this.handleChange.bind(this)}
                error={this.state.validationErrors[name]} />
        )
    }

    private handleSave = () => {
        const movieId = this.movieToEdit?._id;
        if (movieId) {
            saveMovie({ ...this.state.data, _id: movieId });
        } else {
            addMovie(this.state.data);
        }

        this.props.history.push("/movies");
    };

    render() {
        const selectedGenre = this.state.data.genre;
        return (
            <React.Fragment>
                {!this.movieToEdit && <h1>Add new movie</h1>}
                {this.movieToEdit && <h1>Edit movie {this.movieToEdit.title}</h1>}

                <form onSubmit={this.handleSubmit.bind(this)}>
                    {this.renderInput('title', 'Title')}

                    <ListGroup
                        label="Genre"
                        items={this.genres.map(g => ({ text: g.name, value: g._id }))}
                        selectedItem={{ text: selectedGenre.name, value: selectedGenre._id }}
                        onItemChange={(item) => this.setState({ ...this.state, data: { ...this.state.data, genre: { _id: item!.value as string, name: item!.text } } })}
                    />

                    {this.renderInput('numberInStock', 'Number in Stock', 'number')}
                    {this.renderInput('dailyRentalRate', 'Daily Rental Rate', 'number')}

                    <button className="btn btn-primary" >Save</button>
                </form>
            </React.Fragment>
        );
    }

}