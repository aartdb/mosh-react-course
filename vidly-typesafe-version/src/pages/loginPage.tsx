import React from "react";
import { Input } from "../components/common/input";
import Joi from "joi";
import { FormService } from "../services/formService";
import { Link } from "react-router-dom";

interface Account {
    username: string;
    password: string;

}
interface LoginPageState {
    data: Account;
    validationErrors: Partial<Record<keyof Account, string>>;
}

export class LoginPage extends React.Component<{}, LoginPageState> {
    state: LoginPageState = { data: { username: '', password: '' }, validationErrors: {} };

    schema = Joi.object<Account>({
        username: Joi.string().required().label('Username'),
        password: Joi.string().required().label('Password')
    });
    formService = new FormService(this.schema);

    handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        const errors = this.formService.validateForm(this.state.data);
        this.setState({ validationErrors: errors || {} });
        if (errors) return;

        console.log("Submitted");
    }

    handleChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
        const input = e.target;
        const name = input.name as keyof Account;
        const value = input.value;

        this.setState({ ...this.state, data: { ...this.state.data, [name]: value } });

        const errors = { ...this.state.validationErrors };
        const errorMessage = this.formService.validateField(name, value);
        errors[name] = errorMessage ?? undefined;

        this.setState({ validationErrors: errors });
        return true;
    }

    renderInput(name: keyof Account, label: string, type = 'text') {
        return (
            <Input
                label={label}
                name={name}
                type={type}
                value={this.state.data[name]}
                onChange={this.handleChange.bind(this)}
                error={this.state.validationErrors[name]} />
        )
    }

    render() {
        return (
            <React.Fragment>
                <div>
                    <h1>Login</h1>
                    <form onSubmit={this.handleSubmit.bind(this)}>
                        {this.renderInput('username', 'Username')}
                        {this.renderInput('password', 'Password')}
                        <button className="btn btn-primary">Login</button>
                    </form>
                    Don't have an account yet? <Link to="/register">Register new account</Link>
                </div>
            </React.Fragment>
        )
    }
}