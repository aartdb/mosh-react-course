import React from "react";
import { Input } from "../components/common/input";
import Joi from "joi";
import { FormService } from "../services/formService";

interface Account {
    email: string;
    password: string;
    name: string;

}
interface RegisterPageState {
    data: Account;
    validationErrors: Partial<Record<keyof Account, string>>;
}

export class RegisterPage extends React.Component<{}, RegisterPageState> {
    state: RegisterPageState = { data: { email: '', password: '', name: '' }, validationErrors: {} };
    schema = Joi.object<Account>({
        email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: false } }).required().label('Email'),
        password: Joi.string().required().label('Password'),
        name: Joi.string().required().label('Name'),
    });
    formService = new FormService(this.schema);

    handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        const errors = this.formService.validateForm(this.state.data);
        this.setState({ validationErrors: errors || {} });
        if (errors) return;

        console.log("Submitted");
    }

    handleChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
        const input = e.target;
        const name = input.name as keyof Account;
        const value = input.value;

        this.setState({ ...this.state, data: { ...this.state.data, [name]: value } });

        const errors = { ...this.state.validationErrors };
        const errorMessage = this.formService.validateField(name, value);
        errors[name] = errorMessage ?? undefined;

        this.setState({ validationErrors: errors });
        return true;
    }

    renderInput(name: keyof Account, label: string, type = 'text') {
        return (
            <Input
                label={label}
                name={name}
                type={type}
                value={this.state.data[name]}
                onChange={this.handleChange.bind(this)}
                error={this.state.validationErrors[name]} />
        )
    }

    render() {
        return (
            <React.Fragment>
                <div>
                    <h1>Login</h1>
                    <form onSubmit={this.handleSubmit.bind(this)}>
                        {this.renderInput('email', 'Email')}
                        {this.renderInput('password', 'Password', 'password')}
                        {this.renderInput('name', 'Name')}
                        <button className="btn btn-primary">Login</button>
                    </form>

                </div>
            </React.Fragment>
        )
    }
}