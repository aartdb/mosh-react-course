import React, { Component } from 'react';
import { MoviesPage } from './pages/moviesPage';
import { Redirect, Route, Switch } from "react-router-dom";
import { CustomersPage } from './pages/customersPage';
import { RentalsPage } from './pages/rentalsPage';
import { NotFoundPage } from './pages/notFoundPage';
import { Navbar } from './components/navbar';
import { MovieFromPage } from './pages/movieFormPage';
import { LoginPage } from './pages/loginPage';
import { RegisterPage } from './pages/registerPage';

export class App extends Component {
    render() {
        return (
            <React.Fragment>
                <Navbar />
                <main className="container">
                    <Switch>

                        <Redirect from="/" exact to="/movies" />

                        <Route path="/login" component={LoginPage} />
                        <Route path="/register" component={RegisterPage} />
                        <Route path="/movies/:id" component={MovieFromPage} />
                        <Route path="/movies" component={MoviesPage} />
                        <Route path="/movies/new" component={MoviesPage} />
                        <Route path="/customers" component={CustomersPage} />
                        <Route path="/rentals" component={RentalsPage} />
                        <Route path="/not-found" component={NotFoundPage} />
                        <Redirect to="/not-found" />
                    </Switch>
                </main>
            </React.Fragment>
        )
    }
}