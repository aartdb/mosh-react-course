import React from "react";

interface SearchProps {
    value?: string;
    onChange: (newValue: string) => void;
}
export class Search extends React.Component<SearchProps> {
    render() {
        return (
            <div className="form-group">
                <input
                    value={this.props.value}
                    onChange={(e) => this.props.onChange(e.currentTarget.value)}
                    type="text"
                    className="form-control my-3"
                    placeholder="Search..."
                />
            </div>
        );
    }
}