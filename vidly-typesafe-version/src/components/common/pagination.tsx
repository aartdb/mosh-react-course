import * as React from 'react';

export interface PaginateProps {
    currentPage: number;
    amountOfPages: number;
    onPageChange: (page: number) => void;
}

export class Pagination extends React.Component<PaginateProps> {
    render() {
        if (this.props.amountOfPages === 1) return null;
        return (
            <React.Fragment>
                <nav aria-label="Page navigation example">
                    <ul className="pagination">
                        {Array(this.props.amountOfPages).fill(0).map((_, index) => (
                            <li className={`page-item ${this.props.currentPage === index ? 'active' : ''}`} key={index}>
                                <button className="page-link" onClick={() => this.props.onPageChange(index)}>{index}</button>
                            </li>
                        ))}
                    </ul>
                </nav>
            </React.Fragment>
        );
    }
}


