import React from "react";

export type ListGroupItem = { value: string | number, text: string }
export interface ListGroupProps {
    items: Array<ListGroupItem>;
    nullableLabel?: string;
    label?: string;
    name?: string;
    selectedItem: ListGroupItem | null;
    onItemChange: (item: ListGroupItem | null) => void;
}

export class ListGroup extends React.Component<ListGroupProps> {
    onItemChange(item: ListGroupItem['value'] | null) {
        const selectedItem = this.props.items.find(g => g.value === item);

        this.props.onItemChange(selectedItem || null);
    }

    /**
     * Filter the items array to only include unique items
     */
    getUniqueItems() {
        const seen = new Set();

        return this.props.items.filter(item => {
            const duplicate = seen.has(item.value);
            seen.add(item.value);
            return !duplicate;
        });
    }

    render() {
        return (
            <React.Fragment>
                <div className="form-group">
                    {this.props.label && <label htmlFor={this.props.name}>{this.props.label}</label>}
                    <select
                        name={this.props.name}
                        value={this.props.selectedItem?.value}
                        onChange={(e) => this.onItemChange(e.target.value || null)}
                        className="form-select">
                        {this.props.nullableLabel && <option>{this.props.nullableLabel}</option>}
                        {this.getUniqueItems().map(item => (
                            <option key={item.value} value={item.value}>{item.text}</option>
                        ))}
                    </select>
                </div>
            </React.Fragment>
        )
    }
}