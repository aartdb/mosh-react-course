import React from "react";

interface InputProps {
    label: string;
    name: string;
    value: string;
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
    type?: string;
    error?: string;
}
export class Input extends React.Component<InputProps> {
    render() {
        const { label, name, error, ...attributes } = this.props;
        return (
            <div className="form-group">
                <label htmlFor={name}>{label}</label>
                <input
                    {...attributes}
                    name={name}
                    id={name}
                    className="form-control" />

                {error && <div className="alert alert-danger">{error}</div>}
            </div>
        );
    }
}