import * as React from 'react';

export interface LikeProps {
    liked?: boolean;
    onClick?: () => void;
}

export class Like extends React.Component<LikeProps> {
    render() {
        return (
            <React.Fragment>
                <i className={`u-clickable fa ${this.props.liked ? 'fa-heart' : 'fa-heart-o'} `} onClick={this.props.onClick} ></i>
            </React.Fragment>
        );
    }
}


