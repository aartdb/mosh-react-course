import React from "react";
import { NavLink } from "react-router-dom";

interface NavbarProps {

}

export class Navbar extends React.Component<NavbarProps> {

    render() {
        return <nav className="navbar navbar-expand-lg navbar-light bg-light">

            <NavLink to="/" exact className="navbar-brand">Aartly</NavLink>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    <NavLink to="/" exact className="nav-link">Home</NavLink>
                    <NavLink to="/movies" className="nav-link">Movies</NavLink>
                    <NavLink to="/customers" className="nav-link">Customers</NavLink>
                    <NavLink to="/rentals" className="nav-link">Rentals</NavLink>
                    <NavLink to="/login" className="nav-link">Login</NavLink>
                </ul>
            </div>
        </nav>
    }
}