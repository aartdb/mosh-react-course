import React from "react";

export interface TableColumn {
    path?: string;
    label?: string;
    /** Unique key. Will be set to the path if none provided */
    key: string;
    content?: (item: any) => JSX.Element;
}

interface TableHeaderProps {
    columns: Array<TableColumn>;
    onSort: (column: TableColumn) => void;
    sortColumn: TableSortColumn;
}

export class TableHeader extends React.Component<TableHeaderProps> {

    renderSortIcon(column: TableColumn) {
        const { sortColumn } = this.props;
        if (column.path !== sortColumn.path) return null;
        return <i className={`fa fa-sort-${sortColumn.order}`}></i>;
    }

    render() {
        return <thead>
            <tr>
                {this.props.columns.map(column =>
                    <th key={column.key}
                        onClick={() => this.props.onSort(column)}
                        className={`u-clickable`}>{column.label} {this.renderSortIcon(column)}</th>)}
            </tr>
        </thead>
    }
}