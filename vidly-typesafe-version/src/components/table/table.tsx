import React from "react";
import { TableBody } from "./tableBody";
import { TableColumn, TableHeader } from "./tableHeader";

interface TableProps {
    columns: Array<TableColumn>;
    data: Array<Record<any, any>>;
    sortColumn: TableSortColumn;
    onSort: (columnKey: TableColumn['key']) => void;
}
export class Table extends React.Component<TableProps> {
    render() {
        return <table className="table table-hover">
            <TableHeader columns={this.props.columns} sortColumn={this.props.sortColumn} onSort={(tableColumn) => this.props.onSort(tableColumn.key)} />
            <TableBody data={this.props.data} columns={this.props.columns} />
        </table>
    }
}