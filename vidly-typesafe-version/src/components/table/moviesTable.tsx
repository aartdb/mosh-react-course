import * as React from 'react';
import * as MovieService from '../../services/fakeMovieService';
import { Like } from '../common/like';
import { Pagination } from '../common/pagination';
import _ from 'lodash'
import { TableColumn } from './tableHeader';
import { Table } from './table';
import { Link } from 'react-router-dom';

export interface MoviesTableProps {
    movies: Array<MovieService.MovieEntity>;
    onLike: (movie: MovieService.MovieEntity) => void;
    onDelete: (movie: MovieService.MovieEntity) => void;
}

export interface MoviesTableState {
    currentPage: number;
    pageSize: number;
    sortColumn: TableSortColumn;
}

export class MoviesTable extends React.Component<MoviesTableProps, MoviesTableState> {
    movieService = MovieService;
    columns = this.getColumns();
    defaultSortOrder: TableSortColumn['order'] = 'asc';
    state: MoviesTableState = { currentPage: 0, sortColumn: { path: this.columns[0].path!, order: this.defaultSortOrder }, pageSize: 4 };

    componentDidUpdate(prevProps: MoviesTableProps) {
        if (JSON.stringify(prevProps.movies) !== JSON.stringify(this.props.movies)) {
            this.setState({
                currentPage: 0
            })
        }
    }


    getColumns(): Array<TableColumn> {
        const columns: Array<Override<TableColumn, { key?: TableColumn['key'] }>> = [
            { path: 'title', label: 'Title', content: (movie: MovieService.MovieEntity) => <Link to={`/movies/${movie._id}`}>{movie.title}</Link> },
            { path: 'genre.name', label: 'Genre' },
            { path: 'numberInStock', label: 'Stock' },
            { path: 'dailyRentalRate', label: 'Rate' },
            { key: 'liked', content: movie => <Like liked={movie.liked} onClick={() => this.props.onLike(movie)} /> },
            {
                key: 'delete', content: movie => <button onClick={() => this.props.onDelete(movie)}
                    className="btn btn-danger btn-sm">Delete</button>
            }
        ];

        return columns.map(column => {
            // Set the column key to the path if none provided
            if (!column.key) {
                column.key = column.path;
            }
            return column as TableColumn;
        })
    }

    handlePageChange(page: number) {
        this.setState({ currentPage: page });
    }

    handleSort(path: string) {
        const sortColumn = { ...this.state.sortColumn };
        if (sortColumn.path === path) {
            sortColumn.order = sortColumn.order === 'asc' ? 'desc' : 'asc';
        }
        else {
            sortColumn.path = path;
            sortColumn.order = this.defaultSortOrder;
        }

        this.setState({ sortColumn });
    }

    getPaginatedMovies() {
        const sortedMovies = _.orderBy(this.props.movies, [this.state.sortColumn.path], [this.state.sortColumn.order]);

        const start = this.state.currentPage * this.state.pageSize;
        const end = start + this.state.pageSize;
        return sortedMovies.slice(start, end);
    }

    getAmountOfPages() {
        const { movies } = this.props;
        return Math.ceil(movies.length / this.state.pageSize);
    }

    render() {
        const count = this.props.movies.length;
        if (count === 0) {
            return <p>No movies in the database</p>
        }

        const filteredMovies = this.getPaginatedMovies();

        return (
            <React.Fragment>
                <p>Total of {count} movie(s) in the database.</p>

                <Table data={filteredMovies} columns={this.columns} sortColumn={this.state.sortColumn} onSort={this.handleSort.bind(this)} />

                <Pagination amountOfPages={this.getAmountOfPages()} currentPage={this.state.currentPage} onPageChange={(newPage) => this.handlePageChange(newPage)} />
            </React.Fragment>
        );
    }
}

