import React from "react";
import { TableColumn } from "./tableHeader";
import _ from 'lodash'

export interface TableBodyProps {
    data: Array<Record<any, any>>;
    columns: Array<TableColumn>;
}

export class TableBody extends React.Component<TableBodyProps> {
    renderCell(item: TableColumn, column: TableColumn) {
        if (column.content) return column.content(item);
        if (column.path) {
            return _.get(item, column.path)
        }

        return null;
    }

    render() {
        return (
            <tbody>
                {this.props.data.map((item: any) => {
                    return (
                        <tr key={item._id}>
                            {this.props.columns.map(column => {
                                return <td key={item._id + column.key}>{
                                    this.renderCell(item, column)
                                }</td>
                            })}

                        </tr>

                    )
                })}
            </tbody>
        );
    }
}