import Joi from "joi";
import { Input } from "../components/common/input";

export class FormService {
    constructor(private schema: Joi.Schema) { }

    validateForm(data: any) {
        const result = this.schema.validate(data, { abortEarly: false });
        if (!result.error) return null;

        const errors: Record<string, string> = {};
        for (let item of result.error.details) {
            errors[item.path[0]] = item.message;
        }
        return errors;
    }

    validateField(fieldName: string, value: string) {
        const result = this.schema.extract(fieldName)?.validate(value);

        return result.error?.message || null;
    }
}