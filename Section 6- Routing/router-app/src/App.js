import React, { Component } from "react";
import NavBar from "./components/navbar";
import ProductDetails from "./components/productDetails";
import Products from "./components/products";
import NotFound from "./components/notFound";
import Posts from "./components/posts";
import Home from "./components/home";
import Dashboard from "./components/admin/dashboard";
import "./App.css";
import { Redirect, Route, Switch } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <div>
        <NavBar />
        <div className="container">
          <Switch>
            <Route path="/products/:id" component={ProductDetails} />
            <Route
              path={["/products"]}
              render={(props) => <Products {...props} sortBy="newest" />}
            />
            <Route path={["/posts/:year?/:month?"]} component={Posts} />
            <Redirect from="/messages" to="/posts" />
            <Route path={["/admin"]} component={Dashboard} />
            <Route path={["/not-found"]} component={NotFound} />
            <Route path={["/"]} exact component={Home} />
            <Redirect to="/not-found" />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
