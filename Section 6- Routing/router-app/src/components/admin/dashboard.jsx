import React from "react";
import { Route, Switch } from "react-router-dom";
import { Sidebar } from "./sidebar";
import Posts from "./posts";
import Users from "./users";

const Dashboard = ({ match }) => {
  return (
    <div>
      <h1>Admin Dashboard</h1>
      <Sidebar />
      <Switch>
        <Route path={[`/admin/posts`]} component={Posts} />
        <Route path={[`/admin/users`]} component={Users} />
      </Switch>
    </div>
  );
};

export default Dashboard;
