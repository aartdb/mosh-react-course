import { getGenres } from "./genreService";
import { http } from "./http-service";
import config from "../config.json";

const moviesEndpoint = "movies";
function movieUrl(id) {
  return `${config.apiUrl}/${moviesEndpoint}/${id}`;
}

export async function getMovies() {
  const { data: result } = await http.get(`${config.apiUrl}/${moviesEndpoint}`);

  return result;
}

export async function getMovie(id) {
  console.log("movie get", id);
  const { data: result } = await http.get(movieUrl(id));
  return result;
}

export async function saveMovie(movie) {
  if (movie._id) {
    const body = { ...movie };
    delete body._id; // don't leave the id in there

    return await http.put(movieUrl(movie._id), movie);
  }

  return await http.post(`${config.apiUrl}/${moviesEndpoint}`, movie);
}

export async function deleteMovie(id) {
  await http.delete(movieUrl(id));

  return id;
}
