import axios from "axios";
import { toast } from "react-toastify";
import { logger } from "./log-service";

axios.interceptors.response.use(null, (errorMessage) => {
  const statusCode = getStatusCodeFromMessage(errorMessage);
  const expectedError = statusCode >= 400 && statusCode < 500;

  console.log("error from interceptor", errorMessage);

  // Expected error? Just reject the promise
  if (!expectedError) {
    logger.info(errorMessage);
    toast.error(`An unexpected error occurred`);
  }

  return Promise.reject({ message: errorMessage, status: statusCode });
});

/**
 * Something is wrong, so the statuscode needs to be retrieved from the mssage.
 */
function getStatusCodeFromMessage(errorMessage) {
  const statusCodesMatch = String(errorMessage).match("([0-9]{3})");
  return statusCodesMatch.length ? parseInt(statusCodesMatch[0]) : undefined;
}

export const http = {
  get: axios.get,
  post: axios.post,
  put: axios.put,
  patch: axios.patch,
  delete: axios.delete,
};
