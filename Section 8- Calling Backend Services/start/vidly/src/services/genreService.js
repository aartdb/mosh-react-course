import { http } from "./http-service";
import config from "../config.json";

const genreEndpoint = "genres";

export async function getGenres() {
  const { data: result } = await http.get(`${config.apiUrl}/${genreEndpoint}`);

  return result;
}
