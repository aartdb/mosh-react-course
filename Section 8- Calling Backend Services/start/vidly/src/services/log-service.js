export const logger = {
  error: console.error,
  warn: console.warn,
  info: console.info,
};
