import axios from "axios";
import { toast } from "react-toastify";
import { logger } from "./log-service";

axios.interceptors.response.use(null, (error) => {
  const statusCode = error.response && error.response.status;
  const expectedError = statusCode >= 400 && statusCode < 500;

  // Expected error? Just reject the promise
  if (!expectedError) {
    logger.info(error);
    toast.error(`An unexpected error occurred`);
  }

  return Promise.reject(error);
});

export const http = {
  get: axios.get,
  post: axios.post,
  put: axios.put,
  patch: axios.patch,
};
